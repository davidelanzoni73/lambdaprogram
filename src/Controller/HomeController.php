<?php
// src/Controller/HomeController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
   /**
    * @Route("/", name="home")
    * @return Response
    */
    public function home(): Response
    {
        $dataAdesso = date_create(date('Y-m-d H:i:s'));
        $dataLancio = date_create("2023-01-01 12:00:00");
        $tempoRimanente = date_diff($dataAdesso,$dataLancio);

        return $this->render('home.html.twig', [
            'tempoRimanente' => $tempoRimanente
        ]);
    }
}