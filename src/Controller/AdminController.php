<?php
// src/Controller/AdminController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use App\Entity\UserAdmin;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{    
    /**
    * @Route("/", name="home")
    * @return Response
    */
    public function home(
        UserPasswordEncoderInterface $encoder
    ): Response
    {

        $user = new UserAdmin();
        $plainPassword = 'davide';
        $encoded = $encoder->encodePassword($user, $plainPassword);

        return $this->render('admin/home.html.twig', [
        ]);
    }
}